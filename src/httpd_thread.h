/*
 * httpd_thread.h
 *
 *  Created on: 2015年6月15日
 *      Author: Yi
 */

#ifndef HTTPD_THREAD_H_
#define HTTPD_THREAD_H_

void thread_httpd(void *args);

#endif /* HTTPD_THREAD_H_ */
