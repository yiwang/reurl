/*
 * reurl.h
 *
 *  Created on: 2015年6月15日
 *      Author: Yi
 */

#ifndef SRC_REURL_H_
#define SRC_REURL_H_

#include <httpd.h>

void main_loop(options_t *option);

void fw_init(options_t *option);
void fw_uninit(options_t *option);

#endif /* SRC_REURL_H_ */
