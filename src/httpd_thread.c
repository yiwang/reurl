/*
 * httpd_thread.c
 *
 *  Created on: 2015年6月15日
 *      Author: Yi
 */

#include <stdio.h>
#include <time.h>
#include <httpd.h>

#include "httpd_thread.h"
#include "log.h"

void thread_httpd(void *args)
{
	httpd *webserver = (httpd *)args;

	if (httpdReadRequest(webserver) == 0) {
		/*
		 * We read the request fine
		 */
		debugf("Processing request from %s", webserver->clientAddr);
		debugf("Calling httpdProcessRequest() for %s", webserver->clientAddr);
		httpdProcessRequest(webserver);
		debugf("Returned from httpdProcessRequest() for %s", webserver->clientAddr);
	}
	else {
		debugf("No valid request received from %s", webserver->clientAddr);
	}
	debugf("Closing connection with %s", webserver->clientAddr);
	httpdEndRequest(webserver);
}
