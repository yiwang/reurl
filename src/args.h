/**
  args.h
 */

#ifndef ARGS_H
#define ARGS_H

typedef struct {
	const char *addr;
	int port;
	const char *url;
} options_t;

#endif
