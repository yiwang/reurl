/**
  args.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include "args.h"
#include "log.h"
#include "reurl.h"

options_t options;

static const char *help_message =
		"This program is used to redirect user's http to a special URL.\n"
		"usage: reurl -s <gateway address> -p <port> -r <url>\n"
		"\n"
		"for instance: reurl -s 0.0.0.0 -p 1080 -r http://192.168.0.1/index.html\n"
		"\n"
		"  -h, --help            show this help message and exit\n"
		"  -s <gateway address>  \n"
		"  -p <list port>        \n"
		"  -r <url>              \n"
		"  -v                    verbose logging\n"
		"\n";

static void print_help()
{
	printf("%s", help_message);
	exit(1);
}

static void sig_handler(int signo) {
	if (signo == SIGINT)
		exit(1);

	fw_uninit(&options);
}

int main(int argc, char **argv)
{
	options_t *args = &options;
	int enable = 1;
	char *list = NULL;
	int ch;

	memset(args, 0, sizeof(options_t));
	while ((ch = getopt(argc, argv, "h:s:p:r:v")) != -1) {
		switch (ch) {
		case 's':
			args->addr = strdup(optarg);
			break;
		case 'p':
			args->port = atoi(optarg);
			break;
		case 'r':
			args->url = strdup(optarg);
			break;
		case 'v':
			verbose_mode = 1;
			break;
		default:
			print_help();
		}
	}

	if (args->addr == NULL || args->port == 0 || args->url == NULL) {
		print_help();
		return -1;
	}

	signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);

	main_loop(args);
	return 0;
}
